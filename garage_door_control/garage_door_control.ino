/*****************************************************/
//  ESP-01 Garage Door Control
//
//  Controls an ESP-01 based twin-relay module to 
//  open and close a garage door. Receives commands
//  via MQTT messages.
//
//  The Sketch :
//  - Imports secrets.h with your details
//  - Imports device.h with your device details
//  - Connects to WiFi
//  - Publishes state of magnetic switch via MQTT
//  - Receives MQTT messages to activate door
//  - Receives MQTT messages to turn on/off light
//
//  Inspired by on original script by M. Sperry, 12/15/2018
//  ---------------------------------------
//
//  Requires board:
//  - Generic ESP8266 Module
//  
//    File > Preferences > Additional Boards Manager URLs
//      https://arduino.esp8266.com/stable/package_esp8266com_index.json
//
//  Requires libraries:
//  - PubSubClient v2.8.0 by Nick O'Leary
//
//    Sketch > Include Library > Manage Libraries 
//    For information on installing libraries :
//      https://www.arduino.cc/en/Guide/Libraries
//
//  ---------------------------------------
//
//  Matt Hawkins
//  https://www.tech-spy.co.uk/2019/07/battery-powered-esp-01-temperature-sensor/
//  02/12/2020
//
/*****************************************************/

/* Update these two files with your details  */
#include "secrets.h"
#include "device.h"

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//Define some variables to detect first incoming
// MQTT messages
bool boolPowerupDoor=true;
bool boolPowerupLight=true;

//Declaire global variablwes
String strTopic;
String strPayload;
int intState=0;

WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {
  
  delay(2000);
 
  //Serial.println();
  //Serial.println();
  //Serial.print("Connecting to ");
  //Serial.println(WIFI_SSID);
  //Serial.println(WiFi.macAddress());

  /* Set to station mode and connect to network */
  WiFi.mode(WIFI_STA);
  WiFi.hostname(DEVICE_HOSTNAME);
  WiFi.begin(WIFI_SSID, WIFI_PASSWD);

  /* Wait for connection */
  while (WiFi.status() != WL_CONNECTED) {
    //Serial.print(".");
    delay(500);
  }
 
  //Serial.println("");
  //Serial.println("WiFi connected");  
  //Serial.println("IP address: ");
  //Serial.println(WiFi.localIP());

  // Onboard LED
  digitalWrite(ONBOARD_LED_PIN,HIGH);
  
}

// function called to publish the garage door sensor value
void publishData() {

  char status_open[5]="Open";
  char status_close[7]="Closed";

  // Check if door state has changed
  if (digitalRead(DOOR_SENSOR_PIN) && (intState==0))
  {
    // Door has opened
    intState = 1;
    client.publish(MQTT_TOPIC_SENSOR, status_open, true); 
  }
  else if ((!digitalRead(DOOR_SENSOR_PIN)) && (intState==1))
  {
      // Door has closed
      intState = 0;
      client.publish(MQTT_TOPIC_SENSOR, status_close, true);
  }
  
}

void callback(char* topic, byte* payload, unsigned int length) {

  payload[length] = '\0';
  strTopic = String((char*)topic);

  //check for the garage door topic
  if(strTopic == MQTT_TOPIC_DOOR)
    {
      // check to see if the payload is either on or off
      // doesent really matter since a garage door opener
      // only needs a momenary pulse.
      if (String((char*)payload) == "ON" || String((char*)payload) == "OFF")
      {
        if (boolPowerupDoor==false)
        {
          //turn relay 1 on
          Serial.write(0xA0);
          Serial.write(0x01);
          Serial.write(0x01);
          Serial.write(0xA2);
          delay(500);
          //turn relay 1 off
          Serial.write(0xA0);
          Serial.write(0x01);
          Serial.write(0x00);
          Serial.write(0xA1);
        }
      }
      else
      {
        //turn relay 1 off
        Serial.write(0xA0);
        Serial.write(0x01);
        Serial.write(0x00);
        Serial.write(0xA1);
      }
      boolPowerupDoor=false;
    }
    // Check to see if the topic for the garage light is called
    else if (strTopic == MQTT_TOPIC_LIGHT)
    {
      
      // check to see if the payload is either on or off
      // doesent really matter since a garage door opener
      // only needs a momenary pulse.
      if (String((char*)payload) == "ON")
      {
        if (boolPowerupLight==false)
        {
          //turn relay 2 on
          Serial.write(0xA0);
          Serial.write(0x02);
          Serial.write(0x01);
          Serial.write(0xA3);
        }
      }
      else if (String((char*)payload) == "OFF")
      {
        if (boolPowerupLight==false)
        {
          //turn relay 2 off
          Serial.write(0xA0);
          Serial.write(0x02);
          Serial.write(0x00);
          Serial.write(0xA2);
        }
      }
      else
      {
        // Turn relay 2 off
        Serial.write(0xA0);
        Serial.write(0x02);
        Serial.write(0x00);
        Serial.write(0xA2);
      }
      boolPowerupLight=false;
    }
}
 
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    //Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("arduinoClient", MQTT_USER, MQTT_PASSWD)) {
      //Serial.println("connected");
      // Once connected subscribe to topic
      client.subscribe(MQTT_TOPIC_SUBSCRIBE,0);
    } 
    else {
      //Serial.print("failed, rc=");
      //Serial.print(client.state());
      //Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
 
void setup()
{

  pinMode(ONBOARD_LED_PIN,OUTPUT);
  digitalWrite(ONBOARD_LED_PIN,LOW);

  pinMode(DOOR_SENSOR_PIN,INPUT_PULLUP);
  intState=digitalRead(DOOR_SENSOR_PIN);
 
  setup_wifi();

  Serial.begin(MODULE_BAUD, SERIAL_8N1, SERIAL_TX_ONLY);
  
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callback);

  // Uncomment these serial lines if relays don't
  // work to put nuvoton into listening mode.
  // Appears to only be needed once so can be
  // commented out after testing.
  //Serial.write(0x0D);
  //Serial.write(0x0A);
  //Serial.write(0x2B);
  //Serial.write(0x49);
  //Serial.write(0x50);
  //Serial.write(0x44);
  //Serial.write(0x2C);
  //Serial.write(0x30);
  //Serial.write(0x2C);
  //Serial.write(0x34);
  //Serial.write(0x3A);
    
}
 
void loop()
{
  if (!client.connected()) {
    reconnect();
  }

  // Publish sensor data to MQTT
  publishData();
  
  client.loop();
  delay(100);
}
