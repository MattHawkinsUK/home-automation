/*

  ESP-01 Get Chip ID
  
  Extracts the chip ID and prints to the serial monitor.

  The Sketch :
  - Reads chip ID
  - Prints to Serial Monitor

  ---------------------------------------

  File > Preferences > Additional Boards Manager URLs
    https://arduino.esp8266.com/stable/package_esp8266com_index.json

  ---------------------------------------

  Matt Hawkins
  https://www.tech-spy.co.uk/
  21/08/2020

*/

void setup() {

  /* Serial connection */
  Serial.begin(115200);

  delay(200);
  Serial.println("");
  Serial.println("ESP-01 Chip ID :");

  /* Get ChipID as char array */
  char chrChipID[33];
  itoa(ESP.getChipId(),chrChipID,10);

  Serial.println(chrChipID);

}

void loop() {

}
