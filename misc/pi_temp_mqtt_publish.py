#!/usr/bin/python3
#-----------------------------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
# Project : Pi CPU Temperature MQTT Publish
# File    : pi_temp_mqtt_publish.py
#
# Script to send the Pi CPU temperature to MQTT broker.
#
# Author : Matt Hawkins
# Date   : 16/02/2024
# Source : https://bitbucket.org/MattHawkinsUK/home-automation/src/master/misc/
#
# Install instructions here:
# https://www.raspberrypi-spy.co.uk/
#
# gpiozero CPUTemperature reference:
# https://gpiozero.readthedocs.io/en/stable/api_internal.html?#cputemperature
#
#-----------------------------------------------------------
import paho.mqtt.client as mqtt
import gpiozero as gz
import time

def on_connect(client, userdata, flags, reason_code, properties):
  if reason_code==0:
    print(f"Connected with result code {reason_code}")
  if reason_code>0:
    print(f"Error connecting with reason code {reason_code}")

MQTT_SERVER='YOURMQTTIP'
MQTT_PORT=1883
MQTT_USER='YOURMQTTUSER'
MQTT_PASSWORD='YOURPASSWORD'
MQTT_TOPIC='house/temp/pi400'

client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
client.username_pw_set(username=MQTT_USER,password=MQTT_PASSWORD)
client.on_connect = on_connect
client.connect(MQTT_SERVER, MQTT_PORT, 60)

# Get Pi CPU Temperature
cpu = gz.CPUTemperature()
cpu_temp = cpu.temperature
cpu_temp = round(cpu_temp, 1)

# Publish to MQTT broker
result=client.publish(MQTT_TOPIC, payload=cpu_temp, qos=0, retain=False)
if result.rc==mqtt.MQTT_ERR_SUCCESS:
  print(f"Successfully sent {cpu_temp} to {MQTT_TOPIC}")
else:
  print("Error publishing message")

client.disconnect()
